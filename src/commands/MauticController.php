<?php

namespace jotaworks\mautic\commands;

use Yii;
use yii\console\Controller;
use yii\console\ExitCode;
use jotaworks\mautic\models\mautic\ApiFactory;
use jotaworks\mautic\models\mautic\Mautic;

class MauticController extends Controller
{
    /**
     *
     */
    public function actionVersion()
    {
        echo "Requesting Version...\n";

        Yii::info('Console Mautic getVersion');

        $api = ApiFactory::getInstance('contacts');
        $version = $api->getMauticVersion();

        if(!$version) {
            echo "ERROR\n";
        }

        return ExitCode::OK;
    }

    /**
     *
     */
    public function actionGetContact($id = 1)
    {
        echo "Getting contact...\n";

        $contactApi = ApiFactory::getInstance('contacts');
        $contact = $contactApi->get($id);

        var_dump($contact);
        //var_dump($contactApi->getLogger());
        $contactApi->getLogger()->emergency('TST TEST TEST!!!');

        return ExitCode::OK;
    }

    /**
     *
     */
    public function actionListContactFields()
    {
        echo "Getting Contact Fields...\n";

        $mautic = new Mautic();
        $result = $mautic->contactFieldList();
        var_dump($result);

        return ExitCode::OK;
    }

    public function actionGetEmail($id)
    {
        echo "Getting Email $id...\n";

        $mautic = new Mautic();
        $result = $mautic->emailGet($id);
        var_dump($result);

        return ExitCode::OK;
    }

    public function actionDeleteEmail($id)
    {
        echo "Delete Email $id...\n";

        $mautic = new Mautic();
        $result = $mautic->emailDelete($id);
        var_dump($result);

        return ExitCode::OK;
    }

    public function actionListEmails()
    {
        echo "Getting Emails...\n";

        $mautic = new Mautic();
        $result = $mautic->emailsList();
        _d($result);

        return ExitCode::OK;
    }

    public function actionListForms()
    {
        echo "Getting Forms...\n";

        $mautic = new Mautic();
        $result = $mautic->formsList();
        _d($result);

        return ExitCode::OK;
    }

    public function actionGetForm($id)
    {
        echo "Getting Form $id...\n";

        $mautic = new Mautic();
        $result = $mautic->formGet($id);
        var_dump($result);

        return ExitCode::OK;
    }

    public function actionCreateForm()
    {
        echo "Create Dummy Form...\n";

        $data = array(
            'name' => 'test',
            'formType' => 'standalone',
            'description' => 'API test',
            'fields' => array(
                array(
                    'label' => 'field name',
                    'type' => 'text'
                )
            ),
            'actions' => array(
                array(
                    'name' => 'action name',
                    'description' => 'action desc',
                    'type' => 'lead.pointschange',
                    'properties' => array(
                        'operator' => 'plus',
                        'points' => 2
                    )
                )
            )
        );

        $mautic = new Mautic();
        $result = $mautic->formCreate($data);
        _d($result);

        return ExitCode::OK;
    }

    public function actionEditForm()
    {
        echo "Edit Dummy Form...\n";

        $data = array(
            'name' => 'test',
            'formType' => 'standalone',
            'description' => 'API test',
            'fields' => array(
                array(
                    'label' => 'field name',
                    'type' => 'text',
                    'order' => 1,
                ),
                array(
                    'label' => 'Abschicken',
                    'type' => 'button',
                    'order' => 2,
                )
            ),
            'actions' => array(
                array(
                    'name' => 'action name',
                    'description' => 'action desc',
                    'type' => 'lead.pointschange',
                    'properties' => array(
                        'operator' => 'plus',
                        'points' => 2
                    )
                )
            )
        );

        $mautic = new Mautic();
        $result = $mautic->formEdit(4, $data);
        _d($result);

        return ExitCode::OK;
    }

    /**
     * Creates the needed custom fields in Mautic
     */
    public function actionCreateContactFields()
    {
        echo "Create Contact Fields in Mautic...\n";

        $mauticConfig = ApiFactory::getConfig();
        $defs = $mauticConfig['customFields']['spec'];

        $mautic = new Mautic();
        $results = $mautic->contactFieldCreateMulti($defs);

        if($results['hasError']) {
            echo "Error(s) have been reported!\n";
        }

        foreach ($results['results'] as $item) {
            $fieldAlias = '';
            if(isset($item['field'])) {
                $fieldAlias = $item['field']['alias'];
            }

            $errMsg = '';
            $status = 'SUCCESS';
            if($item['hasError']) {
                $status = 'FAILED';
                $errMsg = 'ERR-MSG: ' . $item['errors'][0]['message'];
                $fieldAlias = $item['def']['alias'];
            }

            echo "$status on creating field \"$fieldAlias\". $errMsg\n";
        }

        return ExitCode::OK;
    }

    /**
     * Fetches the leads that are marked as ready to be dispatched
     */
    public function actionProcessWaitingLeads() {
        echo "Process pending leads...\n";

        $mautic = new Mautic();

        $search = 'ld_dispatch_status:"pending"';
        $start = 0;
        $limit = 1;
        $orderBy = '';
        $orderByDir = 'ASC';
        $publishedOnly = false;
        $minimal = true;

        $results = $mautic->contactList($search, $start, $limit, $orderBy, $orderByDir, $publishedOnly, $minimal);
        _c($results);

        echo "Number of results: " . count($results['items']) . "\n";
        return ExitCode::OK;
    }


    public function actionListTags()
    {
        echo "Getting Tags...\n";

        $mautic = new Mautic();
        $result = $mautic->tagsList();
        _d($result);

        return ExitCode::OK;
    }

    public function actionSegmentsImport() {

        $mautic = new Mautic();

        $segments = [
            ["Newsletter","Description...","ESNC NL"],
            ["Newsletter","Description...","CopMa NL"],
            ["Newsletter","Description...","AZO NL"],
            ["Newsletter","Description...","SatMaConf NL"],
            ["Newsletter","Description...","SpaceEx NL"],
            ["Newsletter","Description...","Inno NL"],
            ["Newsletter","Description...","ESABIC NL"],
            ["Newsletter","Description...","AppCamp NL"],
        ];

        foreach ($segments as $segment) {
            $name = $segment[2];
            $description = $segment[0] . ' ' . $segment[1];

            $data = [
                'name' => $name,
                'description' => $description,
                'isPublished' => 1,
            ];

            _d($data);

            $mautic->segmentCreate($data);

            echo "Done!\n\n\n";
        }
    }
}
