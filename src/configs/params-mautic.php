<?php
/**
 * Content Optimizer GmbH, Essen, Germany
 * All rights reserved. Alle Urheberrechte vorbehalten.
 *
 * User: dbelc
 * Date: 19.07.2018
 * Time: 15:30
 */


/**
 * Example config with Credentials
 *
 * You may use it to load it into the Yii application params in configs/params.php
 *
 * return [
 *      'otherParams' => '...',
 *      'mautic' => require __DIR__ . '/params-mautic.php',
 * ]
 *
 * MAKE SURE TO IGNORE THIS FILE AS IT WILL CONTAIN CREDENTIALS.
 * ADDING IT TO VERSION CONTROLL WILL LIKELY RESULT IN GETTING HACKED!
 */
return [
    'auth' => [
        'username' => '...',
        'password' => '...',
    ],
    'apiUrl' => 'https://...',
    'debugModeClient' => false,
];
