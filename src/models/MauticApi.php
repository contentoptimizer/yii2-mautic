<?php

namespace jotaworks\mautic\models;

use Yii;

/**
 * This is the model class for table "mautic_api".
 *
 * @property int $id
 * @property string $label For Display to admin
 * @property string $api_url API Endpoint
 * @property string $api_user
 * @property string $api_password
 * @property string $date_created
 */
class MauticApi extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'mautic_api';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['api_user', 'api_password', 'date_created'], 'required'],
            [['date_created'], 'safe'],
            [['label', 'api_url', 'api_user', 'api_password'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'label' => 'For Display to admin',
            'api_url' => 'API Endpoint',
            'api_user' => 'Api User',
            'api_password' => 'Api Password',
            'date_created' => 'Date Created',
        ];
    }

}
