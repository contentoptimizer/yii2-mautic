<?php
/**
 * Created by PhpStorm.
 * User: dbelc
 * Date: 06.09.2017
 * Time: 14:45
 */

namespace jotaworks\mautic\models\mautic;

use Yii;
use Mautic\MauticApi;
use Mautic\Auth\ApiAuth;
use yii\base\Exception;

class ApiFactory
{
    /**
     * @var \Mautic\Auth\BasicAuth
     */
    public static $apiAuth = null;

    /**
     * @var array
     */
    public static $apiConfig = null;

    /**
     * Get the config for mautic
     */
    public static function getConfig()
    {
        $params = Yii::$app->params;

        if (!isset($params['mautic'])) {
            throw new Exception('No mautic config found!');
        }

        return $params['mautic'];
    }

    /**
     * Get a new Mautic API Client for an context
     *
     * To give an individual config, provide a config key in options:
     *
     * 'config' => [
     *      'auth' => [
     *          'username' => '...',
     *          'password' => '...',
     *      ],
     *      'apiUrl' => 'https://...',
     *      'debugModeClient' => false,
     * ]
     *
     * @param string $apiContext For example 'contacts'
     * @param array $options
     * @return \Mautic\Api\Api
     * @throws Exception
     * @throws \Mautic\Exception\ContextNotFoundException
     */
    public static function getInstance($apiContext = 'contacts', $options = [])
    {
        // Check if an alternative config was provided
        if (isset($options['config'])) {
            $config = $options['config'];
        } else {
            $config = self::getConfig();
        }

        self::$apiConfig = $config;

        // ApiAuth->newAuth() will accept an array of Auth settings
        $settings = array(
            'userName' => $config['auth']['username'],
            'password' => $config['auth']['password'],
        );

        // Initiate the auth object specifying to use BasicAuth
        $initAuth = new ApiAuth();
        $auth = $initAuth->newAuth($settings, 'BasicAuth');

        if (isset($config['debugModeClient']) && $config['debugModeClient'] === true) {
            $auth->enableDebugMode();
        }

        self::$apiAuth = $auth;

        $api = new MauticApi();
        $mauticApi = $api->newApi($apiContext, $auth, $config['apiUrl']);
        $mauticApi->setLogger(new Logger());

        return $mauticApi;
    }

    /**
     * @return \Mautic\Api\Api
     * @throws Exception
     * @throws \Mautic\Exception\ContextNotFoundException
     */
    public static function getContactFields()
    {
        return self::getInstance('contactFields');
    }

    /**
     * @return \Mautic\Api\Api
     * @throws Exception
     * @throws \Mautic\Exception\ContextNotFoundException
     */
    public static function getContacts()
    {
        return self::getInstance('contacts');
    }

    /**
     * @return \Mautic\Api\Api
     * @throws Exception
     * @throws \Mautic\Exception\ContextNotFoundException
     */
    public static function getSegments()
    {
        return self::getInstance('segments');
    }

    /**
     * @return \Mautic\Api\Api
     * @throws Exception
     * @throws \Mautic\Exception\ContextNotFoundException
     */
    public static function getForms()
    {
        return self::getInstance('forms');
    }

    /**
     * @return \Mautic\Api\Api
     * @throws Exception
     * @throws \Mautic\Exception\ContextNotFoundException
     */
    public static function getEmails()
    {
        return self::getInstance('emails');
    }
}
