<?php
/**
 * Created by PhpStorm.
 * User: dbelc
 * Date: 06.09.2017
 * Time: 17:13
 */

namespace jotaworks\mautic\models\mautic;




/**
 * Class Mautic
 *
 * The purpose of this class is to hide the different implementation details of the various entities on Mautic side.
 * And to provide a single unified API interface to the local application.
 *
 * @package app\models\mautic
 */
class Mautic
{

    /**
     * *********************************   CUSTOM FIELDS    *********************************
     **/


    /**
     * Retrieve all defined fields from contact
     *
     * items    Array of fields
     * maxOrder Highest found value of "order" of fields
     *
     * @return array
     * @throws \Mautic\Exception\ContextNotFoundException
     * @throws \yii\base\Exception
     */
    public function contactFieldList()
    {
        $api = ApiFactory::getContactFields();
        $result = $api->getList();

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['fields'];

        // Find max order
        $return['maxOrder'] = 0;
        foreach ($result['fields'] as $item) {
            if ($item['order'] > $return['maxOrder']) {
                $return['maxOrder'] = $item['order'];
            }
        }

        return $return;
    }

    /**
     * Attention:
     * The field 'order' of the API does not refer to some kind of sortkey, but to the ID of an other record!
     *
     * @param array $def
     * @param $insertAfterId
     * @return array
     */
    public function contactFieldCreate(array $def, $insertAfterId)
    {
        $def['order'] = $insertAfterId;

        $api = ApiFactory::getContactFields();
        $result = $api->create($def);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['field'] = $result['field'];

        return $return;
    }

    /**
     * Attention:
     * The field 'order' of the API does not refer to some kind of sortkey, but to the ID of an other record!
     *
     * @param array $defs
     * @return array
     */
    public function contactFieldCreateMulti(array $defs)
    {
        $return = $this->prepareResult();

        $results = [];
        foreach ($defs as $def) {
            $createResult = $this->contactFieldCreate($def, 1);

            if ($createResult['hasError'] === true) {
                $return['hasError'] = true;
                $createResult['def'] = $def;
            }

            $results[] = $createResult;
        }

        $return['results'] = $results;

        return $return;
    }



    /**
     * *********************************   CONTACTS    *********************************
     **/




    public function contactCreate($data)
    {
        $api = ApiFactory::getContacts();
        $result = $api->create($data);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['contact'];

        return $return;
    }

    /**
     * Finds all **identified** contacts
     *
     * @param string $search
     * @param int $start
     * @param int $limit
     * @param string $orderBy
     * @param string $orderByDir
     * @param bool $publishedOnly
     * @param bool $minimal
     * @return array
     */
    public function contactList($search = '', $start = 0, $limit = 0, $orderBy = '', $orderByDir = 'ASC', $publishedOnly = false, $minimal = true)
    {
        $api = ApiFactory::getContacts();
        $result = $api->getIdentified($search, $start, $limit, $orderBy, $orderByDir, $publishedOnly, $minimal);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['contacts'];

        return $return;
    }

    /**
     * Find a contact by id
     *
     * @param int $id
     * @return array
     */
    public function contactGetById($id)
    {
        $api = ApiFactory::getContacts();
        $result = $api->get($id);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['item'] = $result['contact'];

        return $return;
    }

    /**
     * Update a contact by id
     *
     * @param int $id
     * @param array $data
     * @param bool $createIfNotFound
     * @return array
     */
    public function contactEdit($id, $data, $createIfNotFound = false)
    {
        $api = ApiFactory::getContacts();
        $result = $api->edit($id, $data, $createIfNotFound);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['item'] = $result['contact'];

        return $return;
    }


    /**
     * Returns the total number of contacts
     * With filter !is:anonymous
     *
     * @return array
     */
    public function contactCount()
    {
        $api = ApiFactory::getContacts();
        $result = $api->getList('!is:anonymous', 0, 1, '', 'ASC', true);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['count'] = (int)$result['total'];

        return $return;
    }

    /**
     * Returns all segments of an contact
     *
     * @param int $id
     * @return array
     */
    public function contactSegmentsList($id)
    {
        $api = ApiFactory::getContacts();
        $result = $api->getContactSegments($id);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['lists'];

        return $return;
    }

    /**
     * Returns all campaigns of an contact
     *
     * @param int $id
     * @return array
     */
    public function contactCampaignsList($id)
    {
        $api = ApiFactory::getContacts();
        $result = $api->getContactCampaigns($id);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['campaigns'];

        return $return;
    }

    /**
     * Returns all companies of an contact
     *
     * @param int $id
     * @return array
     */
    public function contactCompaniesList($id)
    {
        $api = ApiFactory::getContacts();
        $result = $api->getContactCompanies($id);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['companies'];

        return $return;
    }

    /**
     * Returns all devices of an contact
     *
     * @param int $id
     * @return array
     */
    public function contactDevicesList($id)
    {
        $api = ApiFactory::getContacts();
        $result = $api->getContactDevices($id);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['devices'];

        return $return;
    }

    /**
     * Returns all available owners for an contact
     *
     * @param int $id
     * @return array
     */
    public function contactAvailableOwnersList($id)
    {
        $api = ApiFactory::getContacts();
        $result = $api->getOwners($id);

        $return = $this->prepareResult([]);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result;

        return $return;
    }

    /**
     * Returns all available fields of an contact
     *
     * @param int $id
     * @return array
     */
    public function contactAvailableFieldsList($id)
    {
        $api = ApiFactory::getContacts();
        $result = $api->getFieldList($id);

        $return = $this->prepareResult([]);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result;

        return $return;
    }

    /**
     * Returns all available notes of an contact
     *
     * @param int $id
     * @return array
     */
    public function contactNotesList($id, $search = '', $start = 0, $limit = 0, $orderBy = '', $orderByDir = 'ASC', $publishedOnly = false, $minimal = true)
    {
        $api = ApiFactory::getContacts();
        $result = $api->getContactNotes($id, $search, $start, $limit, $orderBy, $orderByDir, $publishedOnly, $minimal);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['notes'];

        return $return;
    }

    /**
     * Returns all Activities of an contact
     *
     * @param int $id
     * @return array
     */
    public function contactActivityList($id, $search = '', $includeEvents = [], $excludeEvents = [], $orderBy = '', $orderByDir = 'ASC', $page = null, $dateFrom = null, $dateTo = null)
    {
        $api = ApiFactory::getContacts();
        $result = $api->getActivityForContact($id, $search, $includeEvents, $excludeEvents, $orderBy, $orderByDir, $page, $dateFrom, $dateTo);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['events'];
        $return['total'] = $result['total'];
        $return['page'] = $result['page'];
        $return['limit'] = $result['limit'];
        $return['maxPages'] = $result['maxPages'];
        $return['types'] = $result['types'];
        $return['filters'] = $result['filters'];
        $return['order'] = $result['order'];

        return $return;
    }

    /**
     * Returns all Activities of an contact
     *
     * @param string $search String or search command to filter events by
     * @param int $start
     * @param int $limit
     * @param string $orderBy
     * @param string $orderByDir
     * @return array
     */
    public function contactActivitiesOfAll($search = '', $includeEvents = [], $excludeEvents = [], $orderBy = '', $orderByDir = 'ASC', $page = null, $dateFrom = null, $dateTo = null)
    {
        $api = ApiFactory::getContacts();
        $result = $api->getActivity($search, $includeEvents, $excludeEvents, $orderBy, $orderByDir, $page, $dateFrom, $dateTo);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['events'];
        $return['total'] = $result['total'];
        $return['page'] = $result['page'];
        $return['limit'] = $result['limit'];
        $return['maxPages'] = $result['maxPages'];
        $return['types'] = $result['types'];
        $return['filters'] = $result['filters'];
        $return['order'] = $result['order'];

        return $return;
    }



    /**
     * *********************************   EMAILS    *********************************
     **/




    /**
     * Get an email by ID
     */
    public function emailGet($id) {
        $api = ApiFactory::getEmails();
        $result = $api->get($id);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['item'] = $result['email'];

        return $return;
    }

    /**
     * Find all emails
     *
     * @param string $search
     * @param int $start
     * @param int $limit
     * @param string $orderBy
     * @param string $orderByDir
     * @param bool $publishedOnly
     * @param bool $minimal
     * @return array
     */
    public function emailsList($search = '', $start = 0, $limit = 0, $orderBy = '', $orderByDir = 'ASC', $publishedOnly = false, $minimal = false) {
        $api = ApiFactory::getEmails();
        $result = $api->getList($search, $start, $limit, $orderBy, $orderByDir, $publishedOnly, $minimal);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['emails'];

        return $return;
    }

    /**
     * Creates an email
     *
     * @param array $data
     * @return array
     */
    public function emailCreate($data) {
        $api = ApiFactory::getEmails();
        $result = $api->create($data);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['email'];

        return $return;
    }

    /**
     * Edits an email by ID
     *
     * @param int $id
     * @param array $data
     * @param bool $createIfNotFound
     * @return array
     */
    public function emailEdit($id, $data, $createIfNotFound = false) {
        $api = ApiFactory::getForms();
        $result = $api->edit($id, $data, $createIfNotFound);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['item'] = $result['form'];

        return $return;
    }

    /**
     * Deletes an email by ID
     */
    public function emailDelete($id) {
        $api = ApiFactory::getEmails();
        $result = $api->delete($id);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['item'] = $result['email'];

        return $return;
    }


    /**
     * *********************************   FORMS    *********************************
     **/



    /**
     * Find all forms
     *
     * @param string $search
     * @param int $start
     * @param int $limit
     * @param string $orderBy
     * @param string $orderByDir
     * @param bool $publishedOnly
     * @param bool $minimal
     * @return array
     */
    public function formsList($search = '', $start = 0, $limit = 0, $orderBy = '', $orderByDir = 'ASC', $publishedOnly = false, $minimal = false) {
        $api = ApiFactory::getForms();
        $result = $api->getList($search, $start, $limit, $orderBy, $orderByDir, $publishedOnly, $minimal);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['forms'];

        return $return;
    }

    /**
     * Get a forms by ID
     */
    public function formGet($id) {
        $api = ApiFactory::getForms();
        $result = $api->get($id);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['item'] = $result['form'];

        return $return;
    }

    /**
     * Edits a forms by ID
     *
     * @param int $id
     * @param array $data
     * @param bool $createIfNotFound
     * @return array
     */
    public function formEdit($id, $data, $createIfNotFound = false) {
        $api = ApiFactory::getForms();
        $result = $api->edit($id, $data, $createIfNotFound);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['item'] = $result['form'];

        return $return;
    }

    /**
     * Creates a form
     *
     * @param array $data
     * @return array
     */
    public function formCreate($data) {
        $api = ApiFactory::getForms();
        $result = $api->create($data);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['form'];

        return $return;
    }

    /**
     * Deletes a forms by ID
     *
     * @param int $id
     * @return array
     */
    public function formDelete($id) {
        $api = ApiFactory::getForms();
        $result = $api->delete($id);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        return $return;
    }

    /**
     * *********************************   SEGMENTS    *********************************
     **/


    /**
     * Finds all segments
     *
     * @param string $search
     * @param int $start
     * @param int $limit
     * @param string $orderBy
     * @param string $orderByDir
     * @param bool $publishedOnly
     * @param bool $minimal
     * @return array
     */
    public function segmentList($search = '', $start = 0, $limit = 0, $orderBy = '', $orderByDir = 'ASC', $publishedOnly = false, $minimal = true)
    {
        $segmentApi = ApiFactory::getSegments();
        $result = $segmentApi->getList($search, $start, $limit, $orderBy, $orderByDir, $publishedOnly, $minimal);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['lists'];

        return $return;
    }

    /**
     * Create a new segment
     *
     * Example definition:
     * $data = array(
     * 'name'        => 'Segment A',
     * 'alias'       => 'segment-a',
     * 'description' => 'This is my first segment created via API.',
     * 'isPublished' => 1,
     * 'filters' => array(
     * array(
     * 'glue' => 'and',
     * 'field' => 'email',
     * 'object' => 'lead',
     * 'type' => 'email',
     * 'filter' => '*@gmail.com',
     * 'operator' => 'like',
     * ),
     * ),
     * );
     *
     * @see https://developer.mautic.org/?php#create-segment
     * @param array $data
     * @return array
     */
    public function segmentCreate($data)
    {
        $segmentApi = ApiFactory::getSegments();
        $result = $segmentApi->create($data);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['item'] = $result['list'];
        $return['result'] = $result;

        return $return;
    }

    /**
     * Adds a contact to a segment
     *
     * @param int $segmentId
     * @param int $contactId
     * @return array
     */
    public function segmentContactAdd($segmentId, $contactId)
    {
        $segmentApi = ApiFactory::getSegments();
        $result = $segmentApi->addContact($segmentId, $contactId);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['result'] = $result;

        return $return;
    }

    /**
     * Removes a contact from a segment
     *
     * @param int $segmentId
     * @param int $contactId
     * @return array
     */
    public function segmentContactRemove($segmentId, $contactId)
    {
        $segmentApi = ApiFactory::getSegments();
        $result = $segmentApi->removeContact($segmentId, $contactId);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['result'] = $result;

        return $return;
    }


    /**
     * *********************************   TAGS    *********************************
    **/

    /**
     * Find all tags
     *
     * @param string $search
     * @param int $start
     * @param int $limit
     * @param string $orderBy
     * @param string $orderByDir
     * @param bool $publishedOnly
     * @param bool $minimal
     * @return array
     */
    public function tagsList($search = '', $start = 0, $limit = 0, $orderBy = '', $orderByDir = 'ASC', $publishedOnly = false, $minimal = false) {
        $api = ApiFactory::getInstance('tags');
        $result = $api->getList($search, $start, $limit, $orderBy, $orderByDir, $publishedOnly, $minimal);

        $return = $this->prepareResult($result);
        if ($return['hasError']) {
            return $return;
        }

        $return['items'] = $result['tags'];

        return $return;
    }


    /**
     * *********************************   HELPER    *********************************
     **/

    /**
     * Takes the array returned from the API client and checks for errors.
     *
     * @return array
     */
    protected function prepareResult($apiResult = null)
    {
        $return = [
            'hasError' => false,
            'status' => 0,
            'errors' => null
        ];

        if (!is_null($apiResult)) {
            if (isset($apiResult['errors'])) {
                $return['hasError'] = true;
                $return['errors'] = $apiResult['errors'];
            }
        }

        return $return;
    }
}
