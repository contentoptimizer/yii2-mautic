<?php
/**
 * Copyright (c) 2018. Content Optimizer GmbH, Essen, Deutschland
 */

/**
 * Content Optimizer GmbH, Essen, Germany
 * All rights reserved. Alle Urheberrechte vorbehalten.
 *
 * Created by PhpStorm.
 * User: dbelc
 * Date: 22.01.2018
 * Time: 13:36
 */

namespace jotaworks\mautic\models\mautic;


class MauticHelper
{
    /**
     * Transforms a abritrary string into an Mautic conform alias string
     *
     * @param $name
     * @return string
     */
    public static function makeAlias($name = '') {
        $name = trim($name);
        $name = strtolower($name);
        $name = preg_replace("/[^a-z0-9]/", "_", $name);
        $name = preg_replace("/(_+)/", "_", $name);
        $name = preg_replace("/^_+/", "", $name);
        $name = preg_replace("/_+$/", "", $name);
        return $name;
    }
}
