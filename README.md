Mautic API Extension for Yii 2
==============================

This extension provides an API client for [Mautic](http://mautic.org/) with an integration for [Yii framework 2.0](http://www.yiiframework.com).

For license information check the [LICENSE](LICENSE.md)-file.


Notice about the state of development
-------------------------------------

This extension contains code that was extracted from an internal project and needs some more efforts in order to remove
issues and glitches.

DO NOT USE YET!


Installation
------------

The preferred way to install this extension is through [composer](http://getcomposer.org/download/).

Either run

```
php composer.phar require --prefer-dist jotaworks/yii2-mautic
```

or add

```json
"jotaworks/yii2-mautic": "~2.0.0"
```

to the require section of your composer.json.

Usage
-----

Coming soon!
